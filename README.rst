*******
GRIBLIB
*******

A Python toolkit to interface with atmospheric models.

...


Installing this branch
----------------------

It is best if the required packages are first installed with
`conda`:

.. code-block:: console

    conda install numpy xarray cfgrib pyproj pandas xarray


Clone:

.. code-block:: console

    git clone git@gitlab.com:jdassink/griblib.git


Install:

.. code-block:: console

    cd griblib
    pip install -e .


Example Workflow
----------------
