# -*- coding: utf-8 -*-
import os
import re
from setuptools import setup, find_namespace_packages

# Get README and remove badges.
README = open('README.rst').read()
README = re.sub('----.*marker', '----', README, flags=re.DOTALL)

DESCRIPTION = ('A Python toolkit to interface with atmospheric models.')

setup(
    name='griblib',
    version='0.1',
    python_requires='>3.5.0',
    description=DESCRIPTION,
    long_description=README,
    author=[
        'Jelle Assink'
    ],
    author_email='jelle.assink@knmi.nl',
    url='https://gitlab.com/jdassink/griblib',
    download_url='https://gitlab.com/jdassink/griblib.git',
    license='GNU General Public License v3 (GPLv3)',
    packages=find_namespace_packages(include=['griblib.*']),
    keywords=[
        'atmosphere', 'grib', 'xarray',
        'infrasound', 'gravity-waves'
    ],
    entry_points={},
    scripts=[],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: ' +
        'GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Scientific/Engineering',
        'Topic :: Scientific/Engineering :: Atmospheric Science',
        'Operating System :: OS Independent'
    ],
    install_requires=[
        'numpy>=1.16.3',
        'scipy>=1.0.0',
        'pyproj',
        'xarray',
        'cfgrib',
        'pandas'
    ],
    use_scm_version={
        'root': '.',
        'relative_to': __file__,
        'write_to': os.path.join('griblib', 'version.py'),
    },
    setup_requires=['setuptools_scm'],
)
