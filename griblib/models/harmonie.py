# -*- coding: utf-8 -*-
"""
HARMONIE atmosphere model class

.. module:: harmonie

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""
from griblib.models.core import Atmosphere
import numpy as np
import xarray as xr
from copy import deepcopy

class HARMONIE(Atmosphere):
    def __init__(self, cycle=None, **kwargs):
        Atmosphere.__init__(self)
        self.model = 'HARMONIE'
        try:
            self.cycle = int(cycle)
        except:
            self.cycle = ''

        return

    # def _read_grib(self, fid_grib, list_keys, filter_keys, verbose):
    #     """
    #     Function to read HARMONIE GRIB file and return XArray dataset.

    #     Parameters
    #     ----------

    #     fid_grib : `str`
    #         Filename of GRIB file

    #     list_keys : list of `str`
    #         Type of GRIB parameters to be read, such as PV coefficients

    #     filter_keys : `dict`
    #         Dictionary with information necessry to read a variable. Keys can include
    #         `typeOfLevel`, `shortName` and `stepType`.

    #     Returns
    #     -------
    #     ds : `xarray.Dataset`
    #         XArray Dataset containing original model data
    #     """

    #     backend_args = {'read_keys': list_keys, 'filter_by_keys': filter_keys}
    #     if verbose is False:
    #         backend_args['errors'] = 'ignore'

    #     ds = xr.open_dataset(fid_grib, engine='cfgrib', backend_kwargs=backend_args)

    #     return ds

    # def read_grib(self, fid_grib, request, pv_coefficients=True, verbose=True, **kwargs):
    #     """
    #     Wrapper function to combine multiple level types in one dataset

    #     Parameters
    #     ----------

    #     fid_grib : `str`
    #         Filename of GRIB file

    #     request : list of dictionaries
    #         Dictionary entries should consist of 'level_type' 'var_list'
    #         entries. 'var_list' is a list of model parameters to be read
    #         and its entries should correspond to the 'shortName' parameter.
    #         By default all known variables are attempted to be read.
    #         A parameter is skipped if is not present in the file.

    #     pv_coefficients : `Boolean`
    #         Add GRIB PV coefficients to resulting XArray DataSet

    #     Returns
    #     -------
    #     ds : `xarray.Dataset`
    #         XArray Dataset containing original model data
    #     """
    #     print('*'*80)
    #     print('Reading {model} cy{cycle} GRIB file [ {fid} ]'.format(
    #           fid=fid_grib, model=self.model, cycle=self.cycle))
    #     print('')

    #     ds = []
    #     list_keys = []

    #     if pv_coefficients:
    #         print('Reading GRIB PV coefficients')
    #         list_keys.append('pv')

    #     for item in request:
    #         if 'var_list' in item:
    #             for shortName in item['var_list']:
    #                 filter_keys = {'typeOfLevel': item['level_type'],
    #                                'shortName': shortName}

    #                 msg = ('Reading {variable} on {level_type} level'.format(
    #                        variable=filter_keys['shortName'],
    #                        level_type=filter_keys['typeOfLevel']))

    #                 if 'step_type' in item:
    #                     filter_keys['stepType'] = item['step_type']
    #                     msg += ' (stepType {})'.format(item['step_type'])
    #                 if 'level' in item:
    #                     filter_keys['level'] = item['level']
    #                     msg += ' (level {})'.format(item['level'])

    #                 print(msg)
    #                 try:
    #                     dss = self._read_grib(fid_grib, list_keys,
    #                                           filter_keys, verbose)
    #                     ds.append(dss)
    #                 except ValueError as e:
    #                     print(e)
    #                     pass
    #                 except KeyError:
    #                     msg = (' - **ERROR**: variable {variable} not found.'
    #                            ' Skipping...'.format(variable=shortName))
    #                     print (msg)
    #                     pass

    #         # 'Greedy style' request without variable list
    #         else:
    #             filter_keys = {'typeOfLevel': item['level_type']}
    #             msg = ('Reading all variables on {level_type} level'.format(
    #                    level_type=filter_keys['typeOfLevel']))
    #             if 'step_type' in item:
    #                 filter_keys['stepType'] = item['step_type']
    #                 msg += ' (stepType {})'.format(item['step_type'])
    #             if 'level' in item:
    #                 filter_keys['level'] = item['level']
    #                 msg += ' (level {})'.format(item['level'])

    #             print(msg)
    #             try:
    #                 dss = self._read_grib(fid_grib, list_keys,
    #                                       filter_keys, verbose)
    #                 ds.append(dss)
    #             except:
    #                 msg = (' - ERROR reading in level type {level_type}.'
    #                     .format(level_type=filter_keys['typeOfLevel']))

    #     ds = xr.merge(ds, **kwargs)
    #     return ds
