# -*- coding: utf-8 -*-
"""
Standard plotting routines for GRIBLIB

.. module:: plotting

:author:
    Jelle Assink (jelle.assink@knmi.nl)

:copyright:
    2020, Jelle Assink

:license:
    This code is distributed under the terms of the
    GNU General Public License, Version 3
    (https://www.gnu.org/licenses/gpl-3.0.en.html)
"""

def plot_pressure(ds, *args, **kwargs):
    """Plot data on a map."""
    print(ds)
    return